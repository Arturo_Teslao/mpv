-- MALNETO! (ROUGH DRAFT!)
--
-- mpv --script=overlay.lua My_movie.mkv
--
mp.add_key_binding("loadfile", function()
  local overlay_tracks = mp.get_property("audio-tracks")
  local overlay_index = 0
  for i, track in ipairs(overlay_tracks) do
    if track.name:match(".overlay.opus$") then
      overlay_index = i
      break
    end
  end
  if overlay_index == 0 then
    return
  end
  function next_overlay_track()
    if overlay_index > #overlay_tracks then
      mp.set_property("audio_aid", -1)
      return
    end
    mp.set_property("audio-aid", overlay_index)
    overlay_index = overlay_index + 1
  end
  mp.add_key_binding("n", next_overlay_track)
  mp.add_key_binding("<", function() mp.set_property("volume", mp.get_property("volume") - 10) end)
  mp.add_key_binding(">", function() mp.set_property("volume", mp.get_property("volume") + 10) end)
  next_overlay_track()
end)
